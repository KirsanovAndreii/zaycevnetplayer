package com.example.ankir.zaycevnetplayer;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.ankir.zaycevnetplayer.Model.ResponseTop;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ankir on 03.09.2017.
 */


public class ListAdapter extends ArrayAdapter<ResponseTop.Tracks> {

    public ListAdapter(@NonNull Context context, @LayoutRes ArrayList<ResponseTop.Tracks> values) {
        super(context, R.layout.item_top, values);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            rowView = LayoutInflater.from(getContext()).inflate(R.layout.item_top, parent, false);
            holder = new ViewHolder();

            holder.artistName = (TextView) rowView.findViewById(R.id.item_artist);
            holder.songName = (TextView) rowView.findViewById(R.id.item_song);

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        holder.artistName.setText(getItem(position).artistName);
        holder.songName.setText(getItem(position).track);


        return rowView;
    }

    class ViewHolder {

        public TextView artistName;
        public TextView songName;
    }
}

