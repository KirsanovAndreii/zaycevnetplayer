package com.example.ankir.zaycevnetplayer;

import com.example.ankir.zaycevnetplayer.Model.ResponseToken;

import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.QueryMap;

/**
 * Created by ankir on 03.09.2017.
 */

public class RetrofitAuth {

    private static final String ENDPOINT = "https://api.zaycev.net/external";
    private static ApiInterface apiInterface;


    static {
        initialize();
    }

    interface ApiInterface {
        @GET("/auth")

        void getToken(@QueryMap Map<String, String> parameters, Callback<ResponseToken> callback);
    }

    public static void initialize() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getToken(Map<String, String> parameters, Callback<ResponseToken> callback) {
        apiInterface.getToken(parameters, callback);
    }


}
