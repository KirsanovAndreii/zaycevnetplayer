package com.example.ankir.zaycevnetplayer.Model;

import java.util.List;

/**
 * Created by ankir on 04.09.2017.
 */

public class ResponseTop {
    public String page;
    public String pagesCount;
    public List<Tracks> tracks;

    public class Tracks {
        public String id;
        public String track;
        public String artistName;
        public String duration;
    }

}
