package com.example.ankir.zaycevnetplayer;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ankir.zaycevnetplayer.Model.ResponseToken;
import com.example.ankir.zaycevnetplayer.Model.ResponseTop;
import com.example.ankir.zaycevnetplayer.Model.ResponseUrlMP3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity implements MediaPlayer.OnPreparedListener {
    MediaPlayer mediaPlayer = null;
    private ListView listView;
    private ListAdapter listAdapter;
    Map<String, String> parametersAuth = new HashMap<>();
    Map<String, String> parametersTop = new HashMap<>();
    Map<String, String> parametersMP3 = new HashMap<>();
    String tokenAuth; // ключ доступа
    ArrayList<ResponseTop.Tracks> listTop = new ArrayList<>(); //список композиций


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        getTokenHello();

        listView = (ListView) findViewById(R.id.am_listview);
        listAdapter = new ListAdapter(this, listTop);
        listView.setAdapter(listAdapter);

        // выбор песни по клику
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (mediaPlayer != null) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                }
                Toast.makeText(MainActivity.this, "Play  " + listTop.get(position).artistName, Toast.LENGTH_SHORT).show();
                getHTTP_and_PlayMp3(listTop.get(position).id);
            }
        });


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mediaPlayer.stop();
        mediaPlayer.release();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
    }

    // запрос на подключение Hello
    void getTokenHello() {
        RetrofitHello.getToken(new Callback<ResponseToken>() {
            @Override
            public void success(ResponseToken responseToken, Response response) {
                Log.d("---------tokenHELLO=  ", responseToken.token);

                String hash = MD5.md5(responseToken.token + "kmskoNkYHDnl3ol2");
                Log.d("---------HASH=  ", hash);
                parametersAuth.put("hash", hash);
                parametersAuth.put("code", responseToken.token);
                getTokenAuth();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("---------tokenHELLO=  ", "ERROR");
            }
        });


    }

    // заппрос на доступ с использованием ключа
    void getTokenAuth() {
        RetrofitAuth.getToken(parametersAuth, new Callback<ResponseToken>() {
            @Override
            public void success(ResponseToken responseToken, Response response) {
                Log.d("--------tokenAUTH=  ", responseToken.token);
                tokenAuth = responseToken.token;
                getTopList();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("---------tokenAuth=  ", "ERROR");
            }
        });

    }

    //запрос ТОП исполнителей
    void getTopList() {
        parametersTop.put("page", "1");
        parametersTop.put("access_token", tokenAuth);
        RetrofitTop.getTop(parametersTop, new Callback<ResponseTop>() {
            @Override
            public void success(ResponseTop responseTop, Response response) {
                Log.d("---------TOP=  ", responseTop.tracks.get(0).artistName);
                listTop.clear();
                listTop.addAll(responseTop.tracks);
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("---------TOP=  ", "ERROR");
            }
        });

    }
// запрос ссылки на mp3 и проигрыватель
    void getHTTP_and_PlayMp3(String trackPlay) {
        parametersMP3.put("access_token", tokenAuth);
        parametersMP3.put("encoded_identifier", "");
        RetrofitMP3.getMP3(trackPlay, parametersMP3, new Callback<ResponseUrlMP3>() {
            @Override
            public void success(ResponseUrlMP3 responseUrlMP3, Response response) {

                mediaPlayer = new MediaPlayer();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                try {
                    mediaPlayer.setDataSource(responseUrlMP3.url);
                    mediaPlayer.setOnPreparedListener(MainActivity.this);
                    mediaPlayer.prepareAsync(); // might take long! (for buffering, etc)

                } catch (Exception e) {
                    Toast.makeText(MainActivity.this, "Composition is missing", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });


    }
}
