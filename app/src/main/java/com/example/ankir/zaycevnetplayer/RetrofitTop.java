package com.example.ankir.zaycevnetplayer;

import com.example.ankir.zaycevnetplayer.Model.ResponseTop;

import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.QueryMap;

/**
 * Created by ankir on 04.09.2017.
 */

public class RetrofitTop {

    private static final String ENDPOINT = "https://api.zaycev.net/external";
    private static RetrofitTop.ApiInterface apiInterface;

    //  https://api.zaycev.net/external/top?page=%s&access_token=%s
    static {
        initialize();
    }

    interface ApiInterface {
        @GET("/top")
        void getTop(@QueryMap Map<String, String> parameters, Callback<ResponseTop> callback);
    }

    public static void initialize() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(RetrofitTop.ApiInterface.class);
    }

    public static void getTop(Map<String, String> parameters, Callback<ResponseTop> callback) {
        apiInterface.getTop(parameters, callback);
    }
}
