package com.example.ankir.zaycevnetplayer;

import com.example.ankir.zaycevnetplayer.Model.ResponseTop;
import com.example.ankir.zaycevnetplayer.Model.ResponseUrlMP3;

import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.QueryMap;

/**
 * Created by ankir on 07.09.2017.
 */

public class RetrofitMP3 {

    private static final String ENDPOINT = "https://api.zaycev.net/external";
    private static RetrofitMP3.ApiInterface apiInterface;
    //  https://api.zaycev.net/external/top?page=%s&access_token=%s
    //https://api.zaycev.net/external/track/%s/play?access_token=%s&encoded_identifier=%s
    static {
        initialize();
    }

    interface ApiInterface {
        @GET("/track/{track}/play")
        void getMP3(@Path("track") String track, @QueryMap Map<String, String> parameters, Callback<ResponseUrlMP3> callback);
    }

    public static void initialize() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(RetrofitMP3.ApiInterface.class);
    }

    public static void getMP3(String track, Map<String, String> parameters, Callback<ResponseUrlMP3> callback) {
        apiInterface.getMP3(track, parameters, callback);
    }
}
