package com.example.ankir.zaycevnetplayer;

import com.example.ankir.zaycevnetplayer.Model.ResponseToken;

import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;


/**
 * Created by ankir on 03.09.2017.
 */

public class RetrofitHello {

        private static final String ENDPOINT = "https://api.zaycev.net/external";
        private static ApiInterface apiInterface;


        static {
            initialize();
        }

        interface ApiInterface {
            @GET("/hello")
            void getToken(Callback<ResponseToken> callback);
        }

        public static void initialize() {

            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(ENDPOINT)
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();
            apiInterface = restAdapter.create(ApiInterface.class);
        }

        public static void getToken(Callback<ResponseToken> callback) {
            apiInterface.getToken(callback);
        }


    }

